<?php

namespace App\Service;

use App\Entity\User;
use App\Event\CreatedUserEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private EventDispatcherInterface $dispatcher,
    ){}

    public function createUser(User $user): void
    {
        $user->addRole('ROLE_USER');
        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        $event = new CreatedUserEvent($user);
        $this->dispatcher->dispatch($event, CreatedUserEvent::NAME);
    }
}