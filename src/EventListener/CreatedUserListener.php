<?php

namespace App\EventListener;

use App\Event\CreatedUserEvent;
use Doctrine\ORM\EntityManagerInterface;

class CreatedUserListener
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function onUserCreated(CreatedUserEvent $event)
    {
        $now = new \DateTime();

        $user = $event->getUser();
        $user->setCreatedAt($now);
        $user->setUpdatedAt($now);
        $this->em->persist($user);
        $this->em->flush($user);

    }
}